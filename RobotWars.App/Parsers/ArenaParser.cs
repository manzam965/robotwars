﻿namespace RobotWars.App.Parsers
{
    public class ArenaParser : IParser<IArena>
    {
        public IArena Parse(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                return null;
            }

            var spittedText = text.Split();
            var xParseSuccessful = int.TryParse(spittedText[0], out var x);
            var yParseSuccessful = int.TryParse(spittedText[1], out var y);

            if (xParseSuccessful && yParseSuccessful)
            {
                return new Arena(x, y);
            }

            return null;
        }
    }
}
