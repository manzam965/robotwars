﻿namespace RobotWars.App.Parsers
{
    public class RobotParser : IParser<IRobot>
    {
        public IRobot Parse(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                return null;
            }

            var spittedText = text.Split();
            var xParseSuccessful = int.TryParse(spittedText[0], out var x);
            var yParseSuccessful = int.TryParse(spittedText[1], out var y);

            if (!xParseSuccessful || !yParseSuccessful)
            {
                return null;
            }

            var direction = 0;

            switch (spittedText[2].ToUpper())
            {
                case "N":
                    direction = (int)Enums.Direction.North;
                    break;
                case "E":
                    direction = (int)Enums.Direction.East;
                    break;
                case "S":
                    direction = (int)Enums.Direction.South;
                    break;
                case "W":
                    direction = (int)Enums.Direction.West;
                    break;
            }

            return new Robot(x, y, (Enums.Direction)direction);
        }
    }
}
