﻿using RobotWars.Enums;
using System.Collections.Generic;

namespace RobotWars.App.Parsers
{
    public class CommandParser : IParser<IEnumerable<Command>>
    {
        public IEnumerable<Command> Parse(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                return null;
            }

            var chars = text.ToUpper().ToCharArray();
            var commands = new List<Command>();

            foreach (var character in chars)
            {
                switch (character)
                {
                    case 'L':
                        commands.Add(Command.RotateLeft);
                        break;
                    case 'R':
                        commands.Add(Command.RotateRight);
                        break;
                    case 'M':
                        commands.Add(Command.Move);
                        break;
                }
            }

            return commands;
        }
    }
}
