﻿namespace RobotWars.App.Parsers
{
    public interface IParser<out T>
    {
        T Parse(string text);
    }
}
