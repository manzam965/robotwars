﻿using RobotWars.App.Parsers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using RobotWars.Commands;

namespace RobotWars.App
{
    internal static class Program
    {
        private const string InputFilePath = "../Debug/Data/Input.txt";

        private static void Main()
        {
            var input = File.ReadLines(InputFilePath);
            var inputLines = input as IList<string> ?? input.ToList();
            var fistLine = inputLines.First();

            var arenaParser = new ArenaParser();
            var arena = arenaParser.Parse(fistLine);
            var robotParser = new RobotParser();
            var commandParser = new CommandParser();

            var restOfTheLines = inputLines.Skip(1).ToArray();
            var robotNr = 1;

            for (var i = 0; i < restOfTheLines.Length - 1; i += 2)
            {
                var robot = robotParser.Parse(restOfTheLines[i]);
                var isDeploymentSuccessful = arena.DeployRobot(robot);

                if (isDeploymentSuccessful)
                {
                    var commands = commandParser.Parse(restOfTheLines[i + 1]);

                    foreach (var command in commands)
                    {
                        var commandToExecute = CommandFactory.CreateCommand(command, robot);
                        commandToExecute?.Execute();
                    }
                }

                Console.WriteLine($"Robot Nr. {robotNr}");
                Console.WriteLine($"Location X: {robot.Location.X}");
                Console.WriteLine($"Location Y: {robot.Location.Y}");
                Console.WriteLine($"Direction: {robot.Direction}");
                Console.WriteLine();

                robotNr++;
            }
        }
    }
}
