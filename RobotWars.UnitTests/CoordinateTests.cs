using System;
using Xunit;

namespace RobotWars.UnitTests
{
    public class CoordinateTests
    {
        [Fact]
        public void Should_Throw_With_Negative_Arguments()
        {
            Assert.Throws<ArgumentException>(() => new Coordinate(-10, -5));
        }

        [Fact]
        public void Clone_Should_Create_New_Instance()
        {
            var coordinate = new Coordinate(5, 5);
            Assert.NotSame(coordinate, coordinate.Clone());
        }
    }
}
