﻿using System;
using RobotWars.Enums;
using Xunit;

namespace RobotWars.UnitTests
{
    public class RobotTests
    {
        [Fact]
        public void Should_Create_Instance_With_Correct_Location()
        {
            var robot = new Robot(10, 5, Direction.North);
            
            Assert.Equal(10, robot.Location.X);
            Assert.Equal(5, robot.Location.Y);

            var robot2 = new Robot(new Coordinate(8, 5), Direction.North);

            Assert.Equal(8, robot2.Location.X);
            Assert.Equal(5, robot2.Location.Y);
        }
    }
}
