﻿using System;
using RobotWars.Enums;
using Xunit;

namespace RobotWars.UnitTests
{
    public class ArenaTests
    {
        [Fact]
        public void Out_Of_Bounds_Should_Return_True_When_Point_Is_Outside()
        {
            var arena = new Arena(5, 5);
            var isOutOfBounds = arena.IsOutOfBounds(new Coordinate(5, 6));
            Assert.True(isOutOfBounds);

            var isOutOfBounds2 = arena.IsOutOfBounds(new Coordinate(6, 5));
            Assert.True(isOutOfBounds2);

            var isOutOfBounds3 = arena.IsOutOfBounds(new Coordinate(6, 6));
            Assert.True(isOutOfBounds3);
        }

        [Fact]
        public void Out_Of_Bounds_Should_Return_False_When_Point_Is_Inside()
        {
            var arena = new Arena(5, 5);
            var isOutOfBounds = arena.IsOutOfBounds(new Coordinate(3, 3));

            Assert.False(isOutOfBounds);
        }

        [Fact]
        public void Should_Return_True_When_Passing_Null()
        {
            var arena = new Arena(5, 5);
            var isOutOfBounds = arena.IsOutOfBounds(null);

            Assert.True(isOutOfBounds);
        }

        [Fact]
        public void Should_Deploy_Robot_Successfully_When_Location_Is_Valid()
        {
            var robot = new Robot(3, 3, Direction.North);
            var arena = new Arena(5, 5);
            var isDeployedSuccessfully = arena.DeployRobot(robot);

            Assert.True(isDeployedSuccessfully);
            Assert.NotNull(robot.Arena);
            Assert.NotEmpty(arena.Robots);
        }

        [Fact]
        public void Should_Not_Deploy_Robot_When_Location_Is_Valid()
        {
            var robot = new Robot(8, 8, Direction.North);
            var arena = new Arena(5, 5);
            var isDeployedSuccessfully = arena.DeployRobot(robot);

            Assert.False(isDeployedSuccessfully);
            Assert.Null(robot.Arena);
            Assert.Empty(arena.Robots);
        }

        [Fact]
        public void Should_Throw_Exception_When_Lower_Left_Point_Given()
        {
            Assert.Throws<ArgumentException>(() => new Arena(0, 0));
        }
    }
}
