﻿using RobotWars.Commands;
using RobotWars.Enums;
using Xunit;

namespace RobotWars.UnitTests.CommandTests
{
    public class CombinedCommandTests
    {
        [Fact]
        public void Final_Position_Should_Be_Correct()
        {
            var robot = new Robot
            {
                Arena = new Arena(5, 5),
                Direction = Direction.North,
                Location = new Coordinate(1, 2)
            };

            var rotateLeftCommand = new RotateLeftCommand(robot);
            var moveCommand = new MoveCommand(robot);
            rotateLeftCommand.Execute();
            moveCommand.Execute();
            rotateLeftCommand.Execute();
            moveCommand.Execute();
            rotateLeftCommand.Execute();
            moveCommand.Execute();
            rotateLeftCommand.Execute();
            moveCommand.Execute();
            moveCommand.Execute();

            Assert.Equal(1, robot.Location.X);
            Assert.Equal(3, robot.Location.Y);
            Assert.Equal(Direction.North, robot.Direction);
        }

        [Fact]
        public void Final_Position_Should_Be_Correct_2()
        {
            var robot = new Robot
            {
                Arena = new Arena(5, 5),
                Direction = Direction.East,
                Location = new Coordinate(3, 3)
            };

            var rotateRightCommand = new RotateRightCommand(robot);
            var moveCommand = new MoveCommand(robot);
            moveCommand.Execute();
            moveCommand.Execute();
            rotateRightCommand.Execute();
            moveCommand.Execute();
            moveCommand.Execute();
            rotateRightCommand.Execute();
            moveCommand.Execute();
            rotateRightCommand.Execute();
            rotateRightCommand.Execute();
            moveCommand.Execute();

            Assert.Equal(5, robot.Location.X);
            Assert.Equal(1, robot.Location.Y);
            Assert.Equal(Direction.East, robot.Direction);
        }
    }
}
