﻿using RobotWars.Commands;
using RobotWars.Enums;
using Xunit;

namespace RobotWars.UnitTests.CommandTests
{
    public class RotateRightCommandTests : CommandTestsBase
    {
        [Fact]
        public void Should_Point_East_When_Direction_Was_North()
        {
            var robot = CreateRobot(Direction.North);

            var command = new RotateRightCommand(robot);
            command.Execute();

            Assert.Equal(Direction.East, robot.Direction);
        }

        [Fact]
        public void Should_Point_South_When_Direction_Was_East()
        {
            var robot = CreateRobot(Direction.East);

            var command = new RotateRightCommand(robot);
            command.Execute();

            Assert.Equal(Direction.South, robot.Direction);
        }

        [Fact]
        public void Should_Point_West_When_Direction_Was_South()
        {
            var robot = CreateRobot(Direction.South);

            var command = new RotateRightCommand(robot);
            command.Execute();

            Assert.Equal(Direction.West, robot.Direction);
        }

        [Fact]
        public void Should_Point_North_When_Direction_Was_West()
        {
            var robot = CreateRobot(Direction.West);

            var command = new RotateRightCommand(robot);
            command.Execute();

            Assert.Equal(Direction.North, robot.Direction);
        }
    }
}
