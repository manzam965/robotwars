﻿using RobotWars.Commands;
using RobotWars.Enums;
using Xunit;

namespace RobotWars.UnitTests.CommandTests
{
    public class FactoryTests : CommandTestsBase
    {
        [Fact]
        public void Should_Create_Move_Command_When_Required()
        {
            var robot = CreateRobot(Direction.North);

            var command = CommandFactory.CreateCommand(Command.Move, robot);

            Assert.IsType<MoveCommand>(command);
        }

        [Fact]
        public void Should_Create_Rotate_Left_Command_When_Required()
        {
            var robot = CreateRobot(Direction.North);

            var command = CommandFactory.CreateCommand(Command.RotateLeft, robot);

            Assert.IsType<RotateLeftCommand>(command);
        }

        [Fact]
        public void Should_Create_Rotate_Right_Command_When_Required()
        {
            var robot = CreateRobot(Direction.North);

            var command = CommandFactory.CreateCommand(Command.RotateRight, robot);

            Assert.IsType<RotateRightCommand>(command);
        }
    }
}
