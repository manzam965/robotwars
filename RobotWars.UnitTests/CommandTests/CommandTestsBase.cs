﻿using RobotWars.Enums;

namespace RobotWars.UnitTests.CommandTests
{
    public class CommandTestsBase
    {
        protected Robot CreateRobot(Direction direction)
        {
            return new Robot
            {
                Arena = new Arena(5, 5),
                Direction = direction,
                Location = new Coordinate(3, 3)
            };
        }
    }
}
