﻿using RobotWars.Commands;
using RobotWars.Enums;
using Xunit;

namespace RobotWars.UnitTests.CommandTests
{
    public class MoveCommandTests : CommandTestsBase
    {
        [Fact]
        public void Should_Increase_X_When_Direction_Is_East()
        {
            var robot = CreateRobot(Direction.East);

            var moveCommand = new MoveCommand(robot);
            moveCommand.Execute();

            Assert.Equal(4, robot.Location.X);
            Assert.Equal(3, robot.Location.Y);
        }

        [Fact]
        public void Should_Decrease_X_When_Direction_Is_West()
        {
            var robot = CreateRobot(Direction.West);

            var moveCommand = new MoveCommand(robot);
            moveCommand.Execute();

            Assert.Equal(2, robot.Location.X);
            Assert.Equal(3, robot.Location.Y);
        }

        [Fact]
        public void Should_Increase_Y_When_Direction_Is_North()
        {
            var robot = CreateRobot(Direction.North);

            var moveCommand = new MoveCommand(robot);
            moveCommand.Execute();

            Assert.Equal(3, robot.Location.X);
            Assert.Equal(4, robot.Location.Y);
        }

        [Fact]
        public void Should_Decrease_Y_When_Direction_Is_South()
        {
            var robot = CreateRobot(Direction.South);

            var moveCommand = new MoveCommand(robot);
            moveCommand.Execute();

            Assert.Equal(3, robot.Location.X);
            Assert.Equal(2, robot.Location.Y);
        }

        [Fact]
        public void Should_Not_Move_When_Reached_Wall()
        {
            var robot = CreateRobot(Direction.North);

            var moveCommand = new MoveCommand(robot);
            moveCommand.Execute();

            Assert.Equal(4, robot.Location.Y);

            moveCommand.Execute();

            Assert.Equal(5, robot.Location.Y);

            moveCommand.Execute();

            Assert.Equal(5, robot.Location.Y);
        }
    }
}
