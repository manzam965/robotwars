﻿using RobotWars.App.Parsers;
using RobotWars.Enums;
using Xunit;

namespace RobotWars.UnitTests.ParserTests
{
    public class RobotParserTests
    {
        [Fact]
        public void Should_Parse_Correctly_With_Valid_Text()
        {
            var robotParser = new RobotParser();

            var robot = robotParser.Parse("3 3 N");

            Assert.Equal(3, robot.Location.X);
            Assert.Equal(3, robot.Location.Y);
            Assert.Equal(Direction.North, robot.Direction);

            var robot2 = robotParser.Parse("5 5 E");

            Assert.Equal(5, robot2.Location.X);
            Assert.Equal(5, robot2.Location.Y);
            Assert.Equal(Direction.East, robot2.Direction);

            var robot3 = robotParser.Parse("2 2 W");

            Assert.Equal(2, robot3.Location.X);
            Assert.Equal(2, robot3.Location.Y);
            Assert.Equal(Direction.West, robot3.Direction);

            var robot4 = robotParser.Parse("1 1 S");

            Assert.Equal(1, robot4.Location.X);
            Assert.Equal(1, robot4.Location.Y);
            Assert.Equal(Direction.South, robot4.Direction);
        }

        [Fact]
        public void Should_Return_Null_With_Invalid_Text()
        {
            var robotParser = new RobotParser();

            var robot = robotParser.Parse("test test N");

            Assert.Null(robot);

            var robot2 = robotParser.Parse("test 5 N");

            Assert.Null(robot2);
        }

        [Fact]
        public void Should_Return_Null_When_Empty_String_Given()
        {
            var robotParser = new RobotParser();

            var robot = robotParser.Parse(string.Empty);

            Assert.Null(robot);
        }
    }
}
