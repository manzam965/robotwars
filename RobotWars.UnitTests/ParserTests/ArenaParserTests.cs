﻿using RobotWars.App.Parsers;
using Xunit;

namespace RobotWars.UnitTests.ParserTests
{
    public class ArenaParserTests
    {
        [Fact]
        public void Should_Parse_Correctly_With_Valid_Text()
        {
            var arenaParser = new ArenaParser();

            var arena = arenaParser.Parse("5 5");

            Assert.Equal(5, arena.UpperRightPoint.X);
            Assert.Equal(5, arena.UpperRightPoint.X);
            Assert.IsType<Arena>(arena);
        }

        [Fact]
        public void Should_Return_Null_With_Invalid_Text()
        {
            var arenaParser = new ArenaParser();

            var arena = arenaParser.Parse("test test");

            Assert.Null(arena);

            var arena2 = arenaParser.Parse("test 5");

            Assert.Null(arena2);

            var arena3 = arenaParser.Parse("5 test");

            Assert.Null(arena3);
        }

        [Fact]
        public void Should_Return_Null_With_Empty()
        {
            var arenaParser = new ArenaParser();

            var arena = arenaParser.Parse(string.Empty);

            Assert.Null(arena);
        }
    }
}
