﻿using System.Collections;
using System.Linq;
using RobotWars.App.Parsers;
using RobotWars.Enums;
using Xunit;

namespace RobotWars.UnitTests.ParserTests
{
    public class CommandParserTests
    {
        [Fact]
        public void Should_Parse_Correctly_With_Valid_Text()
        {
            var commandParser = new CommandParser();

            var commands = commandParser.Parse("MLRMLR");
            var commandsList = commands.ToList();

            Assert.Equal(6, commandsList.Count);
            Assert.Equal(2, commandsList.Count(c => c == Command.Move));
            Assert.Equal(2, commandsList.Count(c => c == Command.RotateLeft));
            Assert.Equal(2, commandsList.Count(c => c == Command.RotateRight));
        }

        [Fact]
        public void Should_Skip_Invalid_Text()
        {
            var commandParser = new CommandParser();

            var commands = commandParser.Parse("MLRtestMLtestR");

            Assert.Equal(6, commands.Count());
        }

        [Fact]
        public void Should_Return_Null_When_Empty_String_Given()
        {
            var commandParser = new CommandParser();

            var commands = commandParser.Parse(string.Empty);

            Assert.Null(commands);
        }
    }
}
