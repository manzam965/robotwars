﻿using System;
using System.Collections.Generic;

namespace RobotWars
{
    public class Arena : IArena
    {
        public Arena(int x, int y)
        {
            if (x == 0 && y == 0)
            {
                throw new ArgumentException("This is lower left coordinate. Arena could not be created.");
            }

            UpperRightPoint = new Coordinate(x, y);
            Robots = new List<IRobot>();
        }

        public Coordinate UpperRightPoint { get; }
        public List<IRobot> Robots { get; }

        public bool DeployRobot(IRobot robot)
        {
            if (IsOutOfBounds(robot.Location))
            {
                return false;
            }

            robot.Arena = this;
            Robots.Add(robot);

            return true;
        }

        public bool IsOutOfBounds(Coordinate coordinate)
        {
            if (coordinate == null)
            {
                return true;
            }

            return coordinate.X > UpperRightPoint.X || coordinate.Y > UpperRightPoint.Y || (coordinate.X < 0 || coordinate.Y < 0);
        }
    }
}
