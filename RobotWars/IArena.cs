﻿namespace RobotWars
{
    public interface IArena
    {
        Coordinate UpperRightPoint { get; }

        bool DeployRobot(IRobot robot);
        bool IsOutOfBounds(Coordinate coordinate);
    }
}
