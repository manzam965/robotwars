﻿using RobotWars.Enums;

namespace RobotWars
{
    public interface IRobot
    {
        Coordinate Location { get; }
        Direction Direction { get; }
        IArena Arena { set; }

        void Rotate(bool clockwise = true);
        void Move();
    }
}
