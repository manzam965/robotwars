﻿using RobotWars.Enums;

namespace RobotWars
{
    public class Robot : IRobot
    {
        public Robot()
        {
            Location = new Coordinate();
            Direction = Direction.North;
        }

        public Robot(Coordinate coordinate, Direction direction)
        {
            Location = coordinate;
            Direction = direction;
        }

        public Robot(int x, int y, Direction direction)
        {
            Location = new Coordinate(x, y);
            Direction = direction;
        }

        public void Move()
        {
            if (Location.Clone() is Coordinate newLocation)
            {
                switch (Direction)
                {
                    case Direction.North:
                        newLocation.Y++;
                        break;
                    case Direction.West:
                        newLocation.X--;
                        break;
                    case Direction.South:
                        newLocation.Y--;
                        break;
                    case Direction.East:
                        newLocation.X++;
                        break;
                }

                if (!Arena.IsOutOfBounds(newLocation))
                {
                    Location = newLocation;
                }
            }
        }

        public void Rotate(bool clockwise = true)
        {
            var newDirection = clockwise ? Direction + 1 : Direction - 1;

            if (newDirection < 0)
            {
                newDirection = Direction.West;
            }

            if (newDirection > Direction.West)
            {
                newDirection = Direction.North;
            }

            Direction = newDirection;
        }

        public Coordinate Location { get; set; }
        public Direction Direction { get; set; }
        public IArena Arena { get; set; }
    }
}
