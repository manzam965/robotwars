﻿using System;

namespace RobotWars
{
    public class Coordinate : ICloneable
    {
        public Coordinate()
        {
            X = 0;
            Y = 0;
        }

        public Coordinate(int x, int y)
        {
            if (x < 0 || y < 0)
            {
                throw new ArgumentException("Coordinates must be positive");
            }

            X = x;
            Y = y;
        }

        public int X { get; set; }
        public int Y { get; set; }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}
