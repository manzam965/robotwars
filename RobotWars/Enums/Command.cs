﻿namespace RobotWars.Enums
{
    public enum Command
    {
        Move,
        RotateLeft,
        RotateRight
    }
}
