﻿namespace RobotWars.Enums
{
    public enum Direction
    {
        North,
        East,
        South,
        West
    }
}
