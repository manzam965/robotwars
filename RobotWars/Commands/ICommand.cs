﻿namespace RobotWars.Commands
{
    public interface ICommand
    {
        void Execute();
    }
}
