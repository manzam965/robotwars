﻿namespace RobotWars.Commands
{
    public class RotateRightCommand : ICommand
    {
        private readonly IRobot _robot;

        public RotateRightCommand(IRobot robot)
        {
            _robot = robot;
        }

        public void Execute()
        {
            _robot.Rotate();
        }
    }
}
