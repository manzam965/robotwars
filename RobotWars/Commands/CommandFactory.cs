﻿using RobotWars.Enums;

namespace RobotWars.Commands
{
    public static class CommandFactory
    {
        public static ICommand CreateCommand(Command command, IRobot robot)
        {
            switch (command)
            {
                case Command.Move:
                    return new MoveCommand(robot);
                case Command.RotateLeft:
                    return new RotateLeftCommand(robot);
                case Command.RotateRight:
                    return new RotateRightCommand(robot);
                default:
                    return null;
            }
        }
    }
}
