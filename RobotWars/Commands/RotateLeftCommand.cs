﻿namespace RobotWars.Commands
{
    public class RotateLeftCommand : ICommand
    {
        private readonly IRobot _robot;

        public RotateLeftCommand(IRobot robot)
        {
            _robot = robot;
        }

        public void Execute()
        {
            _robot.Rotate(false);
        }
    }
}
